=== HR Soundifier ===
== Requirements ==
The Soundifier needs the following files:
- hr_soundifier.exe        => This is the application itself
- resources\               => A folder for keeping the other stuff the application needs
  - heartbeat.wav          => The sound file used for the "heartbeat"
  - oggenc2.exe            => Tool used for compressing the audio
  - LICENSE.txt            => The license of this application

= Heartbeat.wav =
This file is used as the "heartbeat" sound in the generated sound file. You can replace the default file with a custom .wav file, as long as it fulfills the following requirements (if you don't know if it does: no worries. Using a wrong one won't break anything, but it may not work and the application will probably tell you what's wrong):
    * 1 or 2 Channels, meaning Mono or Stereo
    * 44,1kHz sample rate
    * Encoded via 16-bit PCM

= oggenc2.exe =
This is the encoder used to compress the audio. By default, the Soundifier emits .wav files, which are huge, so the oggenc is used to compress them into ogg vorbis files.
This tool should be included, otherwise, it can be aquired at https://www.rarewares.org/ogg-oggenc.php - choose the "Generic" variant and extract the oggenc2.exe file into the "resources" folder.
!CAUTION! If this file isn't found, Soundifier will log it as an error, but will continue to function: It will just copy the uncompressed .wav and use it instead. So, if you're missing the expected .ogg files but instead have an output.wav: Check this file.

= LICENSE.txt =
Contains a copy of the Apache 2.0 License, which applies for this application. Please keep this file in here when redistributing this application. This is an open source application, provided free of charge. Please don't be a dick. Honour the license. 


== Usage ==
The application can be used in two main ways: Drag&Drop or launching it directly. Either way, you need to give the application the timings you want to use.
Use the following as an example:
500-630-677-600-550-950
The numbers represent the delay between heartbeats in milliseconds. They are seperated by hyphens (spaces around the hyphens don't matter, you can leave them in).

= Drag&Drop =
Drag&Drop a text file containing the timing data onto the hr_soundifier.exe. A terminal window will open, which will automatically close once the the sound file was generated successfully. If there was an error, it should stay open, showing logs about its operation.

= Direct launch =
Start the hr_soundifier.exe directly (or via a terminal window like cmd / powershell). A terminal window will open, asking for the timing data. You can enter it manually or copy-paste it into the terminal window. After that, the application works the same as with Drag&Drop.

== Troubleshooting ==
= !LOGS! =
This application logs its actions. These logs can be seen either in the terminal window or in the "hr_soundifier.log" file in the "resources" folder. Lines beginning with [INFO] or [DEBUG] are just for information. Lines starting with [WARN] or [ERROR] may be cause for concern.

There's a way to enable more fine-grained logging (tracing): Create a file (any file, the contents don't matter) called "ENABLE_TRACE" next to the .exe. This is mostly just used for sending the developer (hi) interesting logs for figuring out what's going on in case of a weird error.