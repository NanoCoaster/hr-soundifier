use std::{fs::{self, File},
    ffi::OsStr,
    path::PathBuf,
    io::{self, BufReader},
    process::Command,
};
use hound::{self, WavReader};
use anyhow::{Context, Result, anyhow};
use log::{error, warn, info, debug, trace};
use simplelog::*;
use lazy_static::lazy_static;

mod local {
    pub static CANT_READ_TIMINGS_FILE: &str = "Can't read source file for the timings, it may be corrupt.";
    pub static NO_TIMINGS_IN_FILE: &str = "No timings found in the source. Please ensure the file contains exactly one timing per line.";
    pub static NO_SOUND_FILE: &str = "Please ensure there's a \"heartbeat.wav\" file in the \"resources\" directory.";
    pub static CORRUPT_SOUND_FILE: &str = "The \"heartbeat.wav\" file seems to be corrupt.";
    pub static CANT_CREATE_OUTPUT_FILE: &str = "Unable to create the output .wav file. Maybe an old one is being used by an application?";
    pub static CANT_INIT_LOGGING: &str = "Can't even initialize terminal logging. Shit's fucked. Done. FUBAR. Contact Nano, I guess?";
    pub static CANT_CREATE_LOG_FILE: &str = "Can't create log file.";
    pub static CANT_READ_TIMINGS_FROM_TERMINAL: &str = "Can't read timings from terminal. This is pretty much FUBAR, contact Nano.";
    pub static ERROR_PLAYING_OUTPUT_FILE: &str = "Error starting the default media player and playing the output file";
    pub static MISSING_OGGENC: &str = "Missing oggenc2.exe. This tool is needed for converting the .wav to a .ogg file. Please ensure there's a oggenc2.exe in the \"resources\" directory. 
If for some reason you can't find it, it can be downloaded at https://www.rarewares.org/ogg-oggenc.php ; Choose the \"Generic\" variant, extract it and copy the .exe into the \"resources\" directory.";
}

type WavFileWriter = hound::WavWriter<io::BufWriter<fs::File>>;

#[derive(Debug, Clone)]
enum TimingsSource {
    File(PathBuf),
    Stdin,
}

static SAMPLE_RATE: u64 = 44100;
static SAMPLES_PER_MILLISECOND: u64 = SAMPLE_RATE / 1000;

static WAV_SPEC: hound::WavSpec = hound::WavSpec {
    channels: 1,
    sample_rate: SAMPLE_RATE as u32,
    bits_per_sample: 16,
    sample_format: hound::SampleFormat::Int,
};

lazy_static! {
    static ref RESOURCES_PATH: PathBuf = {
        let mut path = std::env::current_exe().expect("Can't find the .exe path. Guru meditation.");
        path.pop();    
        path.push("resources");
        path
    };
    static ref EXE_PATH: PathBuf = {
        let mut path = std::env::current_exe().expect("Can't find the .exe path. Guru meditation.");
        path.pop();
        path
    };
}

fn initialize_logging() {
    let log_level = {
        let mut trace_file_path = EXE_PATH.clone();
        trace_file_path.push("ENABLE_TRACE");
        
        match fs::metadata(trace_file_path) {
            Ok(_) => { 
                println!("Trace enabled!"); 
                LevelFilter::Trace
            }
            _ => LevelFilter::Debug
        }
    };

    let log_file = {
        let mut path = RESOURCES_PATH.clone();
        path.push("hr_soundifier.log");
        File::create(path).expect(local::CANT_CREATE_LOG_FILE)
    };

    CombinedLogger::init(
        vec![
            TermLogger::new(log_level, Config::default(), TerminalMode::Mixed).expect(local::CANT_INIT_LOGGING),
            WriteLogger::new(log_level, Config::default(), log_file),
        ]
    ).expect(local::CANT_INIT_LOGGING);
}

fn main() {
    initialize_logging();

    let args: Vec<String> = std::env::args().collect();

    let timings_source = match args.get(1) {
        Some(path) => TimingsSource::File(PathBuf::from(path)),
        None => TimingsSource::Stdin
    };

    match do_main(timings_source) {
        Err(e) => {
            error!("{}", e);
            io::stdin().read_line(&mut String::new()).expect("Something went horribly wrong.");
        }
        _ => info!("Finished successfully.")
    };
}

fn do_main(timings_source: TimingsSource) -> Result<()> {
    info!("Application started");
    debug!("Sample rate: {}; Samples per millisecond: {}", SAMPLE_RATE, SAMPLES_PER_MILLISECOND);

    info!("Reading timings...");
    let timings = get_times(timings_source.clone())?;
    debug!("Timings loaded: {:?}", timings);

    info!("Loading heartbeat sound sample...");
    let wav_samples = get_sound(RESOURCES_PATH.clone())?;

    info!("Creating output file...");
    let mut wav_output_path = RESOURCES_PATH.clone();
    wav_output_path.push("output.wav");
    let mut writer = hound::WavWriter::create(wav_output_path.clone(), WAV_SPEC).context(local::CANT_CREATE_OUTPUT_FILE)?;

    info!("Writing output data...");
    write_data(&mut writer, &timings, &wav_samples)?;

    info!("Finalizing writing...");
    writer.finalize().context(local::CANT_CREATE_OUTPUT_FILE)?;

    info!("Compressing audio...");
    let ogg_output_path = get_output_path(timings_source);
    debug!("Output path: {:?}", ogg_output_path.clone());

    let final_output_path = match compress_audio(ogg_output_path.clone(), wav_output_path.clone()) {
        Ok(_) => ogg_output_path,
        Err(e) => {
            error!("{}", e);
            warn!("Error compressing audio. Using uncompressed .wav file...");

            let mut wav_final_path = EXE_PATH.clone();
            wav_final_path.push("output.wav");
            
            fs::copy(wav_output_path, wav_final_path.clone()).context("Error copying .wav to base directory")?;
            wav_final_path
        }
    };
    
    info!("Playing output file with default media player...");
    opener::open(final_output_path).context(local::ERROR_PLAYING_OUTPUT_FILE)?;

    Ok(())
}


fn get_times(source: TimingsSource) -> Result<Vec<u64>> {
    let timings_str = match source {
        TimingsSource::File(path) => {
            match fs::canonicalize(PathBuf::from(path.clone())) {
                Ok(p) => info!("Timings file argument found, reading: {:?}", p),
                Err(e) => warn!("Invalid source filepath: {:?}; error: {}", path, e)
            };
            fs::read_to_string(path).context(local::CANT_READ_TIMINGS_FILE)?
        }
        TimingsSource::Stdin => {
            info!("No timings file argument found, reading from stdin...");
            println!("Please enter the timings, one per line:");
            let mut timings = String::new();
            loop {
                let mut buffer = String::new();
                io::stdin().read_line(&mut buffer).context(local::CANT_READ_TIMINGS_FROM_TERMINAL)?;
                timings.push_str(&buffer);
                if buffer.replace("\r", "").starts_with("\n") {
                    break;
                }
            }
            
            timings
        }
    };

    debug!("Got timings string: '{}'", timings_str);

    get_times_from_string(timings_str)
}

fn get_times_from_string(times_str: String) -> Result<Vec<u64>> {
    let times: Vec<u64> = times_str
        .replace("\r", "")
        .split("\n")
        .filter_map(|s| s.trim().parse::<u64>().ok())
        .collect();

    if times.len() == 0 {
        Err(anyhow!(local::NO_TIMINGS_IN_FILE))
    } else {
        Ok(times)
    }
}


fn get_sound(mut path: PathBuf) -> Result<Vec<i16>> {
    path.push("heartbeat.wav");
    let file = File::open(path).context(local::NO_SOUND_FILE)?;
    let reader = BufReader::new(file);

    let mut wav_reader = WavReader::new(reader).context(local::CORRUPT_SOUND_FILE)?;
    let spec = wav_reader.spec();

    if spec.sample_rate as u64 != SAMPLE_RATE {
        return Err(anyhow!("Please use a heartbeat.wav with a sample rate of 44,1kHz"));
    }
    if spec.bits_per_sample != 16 {
        return Err(anyhow!("Please use a heartbeat.wav encoded with 16 bits per sample"));
    }
    if spec.sample_format != hound::SampleFormat::Int {
        return Err(anyhow!("Please use a heartbeat.wav with Integer PCM sample format"));
    }

    let wav_samples: Vec<i16> = wav_reader
        .samples::<i16>()
        .filter_map(|x| x.ok())
        .collect();

    match spec.channels {
        2 => {
            let wav_samples: Vec<i16> = wav_samples
                .chunks(2)
                .map(|x| x[0])
                .collect();
        
            Ok(wav_samples)  
        }
        _ => Ok(wav_samples)
    }      
}

fn write_data(writer: &mut WavFileWriter, timings: &[u64], wav_samples: &[i16]) -> Result<()> {
    let lost_samples = wav_samples.len() as u64;
    debug!("Lost samples: {}", lost_samples);

    for (times_index, delay_time) in timings.iter().enumerate() {
        let delay_samples = (delay_time * SAMPLES_PER_MILLISECOND).saturating_sub(lost_samples);
        trace!("delaying for {} samples; delay time at index:[{}]: {}ms", delay_samples, times_index, delay_time);
        
        for _ in 0..delay_samples {
            writer.write_sample(0)?;
        }
        trace!("Current duration: {} samples, {}ms", writer.duration(), writer.duration() as u64 / SAMPLES_PER_MILLISECOND);

        let stop_after_samples = match timings.get(times_index + 1) {
            Some(next_time) if lost_samples > (*next_time * SAMPLES_PER_MILLISECOND) => *next_time * SAMPLES_PER_MILLISECOND,
            _ => lost_samples,
        };

        trace!("writing heartbeat sound for {} samples", stop_after_samples);
        for s in &wav_samples[0..stop_after_samples as usize] {
            writer.write_sample(*s)?;
        }
        trace!("Current duration: {} samples, {}ms", writer.duration(), writer.duration() as u64 / SAMPLES_PER_MILLISECOND);
    }

    Ok(())
}

fn get_output_path(timings_source: TimingsSource) -> PathBuf {
    let mut ogg_output_path = EXE_PATH.clone();

    if let TimingsSource::File(path) = timings_source {
        let mut path = path.file_stem().unwrap()
            .to_str().unwrap_or("output")
            .to_string();

        path.push_str(".ogg");
        ogg_output_path.push(path);
    } else {
        ogg_output_path.push("output.ogg");
    }   

    ogg_output_path
}

fn compress_audio(output_path: PathBuf, input_path: PathBuf) -> Result<()> {
    let mut oggenc_path = RESOURCES_PATH.clone();
    oggenc_path.push("oggenc2.exe");

    let output_path = output_path.to_str().context("Invalid .ogg output path")?;
    let input_path = input_path.to_str().context("Invalid .wav path")?;

    let output = Command::new(oggenc_path)
        .args(&[input_path, "-q 5", "-o", output_path])
        .output();

    match output {
        Err(ref e) if e.kind() == io::ErrorKind::NotFound => Err(anyhow!(local::MISSING_OGGENC)),
        Err(e) => Err(anyhow!("Unknown error: {}", e)),
        Ok(output) => {
            if !output.status.success() {
                Err(anyhow!("Error compressing audio file: {}", std::str::from_utf8(&output.stderr).unwrap_or("unknown error")))
            } else {
                Ok(())
            }  
        }
    }      
}